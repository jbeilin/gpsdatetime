https://medium.com/@joel.barmettler/how-to-upload-your-python-package-to-pypi-65edc5fe9c56

Upload your package to PyPi

Now, the final step has come: uploading your project to PyPi. First, open the command prompt and navigate into your the folder where you have all your files and your package located:

```
cd "C://PATH//TO//YOUR//FOLDER"
```

Now, we create a source distribution with the following command:

```
python3 setup.py sdist
```

You might get a warning stating “Unknown distribution option: ‘install_requires’. Just ignore it.

We will need twine for the upload process, so first install twine via pip:

```
pip install twine
```

Then, run the following command:

```
twine upload dist/*
```

You will be asked to provide your username and password. Provide the credentials you used to register to PyPi earlier.

Congratulations, your package is now uploaded! Visit https://pypi.org/project/YOURPACKAGENAME/ to see your package online!
