# Gpsdatetime : gestion des temps dans les calculs GNSS

## Constructeur

### Constructeur avec initialisation par défaut

Par défaut le constructeur initialise un objet gpsdatetime à la date/heure correspondant à l'instant où le code est exécuté. 

```python
Python 3.5.2
$ import gpsdatetime as gpst
$ t = gpst.gpsdatetime()
```


### Initialisation d'une date

Il est possible de réinitialiser un objet \tgps à une nouvelle date/heure après avoir appelé le constructeur. Pour cela, différentes méthodes ont été développées. Ces méthodes ont un intitulé se terminant toujours par "\_t".



- just\_now : initialisation à l'instant d'exécution.

```python
$ t.just_now()
```

- ymdhms\_t : année, mois, jour, heure, minute, seconde.

```python
$ t.ymdhms_t(2012,1,9,12,0,0)
```

- yyyyddds\_t : année, jours dans l'année (DOY), seconde de jour.

```python
$ t.yyyyddds_t(2012,9,43200)
```

- gpswkd\_t : semaine GPS, jour dans la semaine [0..6]. La partie fractionnaire représente un jour. décimal.

```python
$ t.gpswkd_t(1400,1.5)
```

- gpswks\_t : semaine GPS, seconde dans la semaine

```python
$ t.gpswks_t(1400,86400.5)
```

- mjd\_t : Modified Julian Date (décimal).

```python
$ t.mjd_t(54800.5)
```

- jd\_t : Julian Date (décimal).

```python
$ t.jd_t(2054800.5)
```

- iso\_t : date au format ISO.

```python
$ t.iso_t('2017-10-09T12:00:00Z')
```

- rinex\_t : date au format Rinex (fichiers d'observations).

```python
$ t.rinex_t('17  6 13  7 35  0.0000000')
```

- snx\_t : date au format Sinex (chaîne de caractères comprenant l'année à 2 chiffres, le jour dans l'année [0..366] et la seconde de jour [0..86400]).

```python
$ t.snx_t('17:204:43200')
```


### Constructeur avec initialisation à une date fournie


Lors de l'instanciation d'un objet \tgps, il est possible de passer un nombre variable de paramètres. Si on passe des paramètres au constructeur, il doivent, sauf cas particulier, impérativement être nommés.

 Différents cas sont possibles :

- si on ne passe aucun paramètre (où si les paramètres ci-dessous sont passés de manière incorrecte), alors l'objet \tgps est instancié avec la date système de la machine à l'heure UTC.

```python
$ t=gpsdatetime()
```

- instanciation à partir d'une mjd.

```python
$ t=gpsdatetime(mjd=54605.678)
```

- l'objet est instancié à yyyy/doy. les secondes dans la journée sont ajoutées ensuite. Ce paramètre est optionnel. 

```python
t=gpsdatetime(yyyy=2016, doy=004, dsec=45677)
```


- L'objet est dans un premier temps initialisé au début de la semaine 1400. Les secondes dans la semaine sont ajoutés ensuite. Ce paramètre est optionnel. 

```python
t=gpsdatetime(wk=1400, wsec=600700)
```

- L'objet est dans un premier temps initialisé au début de la semaine 1400. Le jour puis les secondes dans le jour sont ajoutés successivement. Ces paramètres sont optionnels mais les secondes dans la journées ne sont prises en compte que si le jour dans la semaine est fourni. 

```python
t=gpsdatetime(wk=1400, wd=1, dsec=46888) 
```

- l'objet est instancié à yyyy/mon/day. Les heures, minutes et secondes sont optionnels.

```python
t=gpsdatetime(yyyy=2016, mon=1, dd=7, hh=3, min=5, sec=5)
```

-  t1 étant lui-même un objet gpsdatetime, on instancie une copie t2 de l'objet t1.

```python
t2=gpsdatetime(t1)
```

- instanciation à partir d'une date SINEX.

```python
t=gpsdatetime('16:004:46888')
```

- instanciation à partir d'une date iso.

```python
t=gpsdatetime('16:01:04T03:05:05Z')
```

- instanciation à partir d'une date rinex.

```python
t=gpsdatetime('18 10  9 12 20 45.00000')
```

![parametres](./img/paramInit.png)


## Attributs de la classe gpsdatetime

| Champs  | Description     | Unité |
| :--------------- |:---------------| -----|
|s1970  | secondes depuis 01-01-1970 à 0h00Z | seconde |
|mjd  | Modified Julian Date \newline (MJD = JD – 2400000.5) | jours décimaux |
|jd  | Julian Date | jours décimaux |
|jd50  | Julian Date depuis J1950 | jours décimaux |
|wk  | Semaine GPS | semaine I4 |
|wsec  | seconde dans la semaine GPS | float [0..604800]  |
|yyyy  | année sur 4 chiffres | année  I4 [1902..2079] |
|yy  | année sur 2 chiffres | année (I2) |
|mon  | mois de 1 à 12 | I2  [1..12]  |
|dd  | jour dans le mois | I2 [1..31] |
|hh  | heure | I2 [0..24] |
|min  | minute | I2 [0..60] |
|sec  | seconde | I2 [0..60] |
|doy  | jour dans l'année | I3 [1..366] |
|wd  | jour dans la semaine | I1 [0..6] |
|dsec  | seconde dans la journée | float [0..86400] |
|dy  | année décimale | float [0..366]  |
|GMST  | Greenwich Mean Sidereal Time | heures décimales |
|EQEQ  | Equation des Equinoxes | heures décimales |
|GAST  | Greenwich Aparent Sidereal Time | heures décimales |

	
Pour accéder à un attribut :
```python
$ m1 = t.mjd
```



## Ajout/soustraction de durées

- Surcharge d'opérateur

```python
$ t += 5.0 # ajout de 5s
$ t -= 2.0 # soustraction de 2s
```
-  add\_day : ajout/soustraction de jours décimaux

```python
$ t.add_day(1.5)
```
- add\_h : ajout/soustraction d'heures décimales

```python
$ t.add_h(0.5)
```
- add\_s : ajout/soustraction de secondes décimales

```python
$ t.add_s(-0.5)
```



## Calcul de durée entre 2 objets Gpsdatetime

```python
$ t1 = gpst.gpsdatetime()
$ t2 = gpst.gpsdatetime()
$ Delta_t  = t2 - t1 # resultat en secondes
```

## Comparaison de 2 objets Gpsdatetime

```python
$ t1 = gpst.gpsdatetime()
$ t2 = gpst.gpsdatetime()
$ if t1 < t2:
$ ...print("t1 est avant t2")
```

## Calcul de début de semaine/jour/heure/minute


- wk00 : calcul du début de la semaine

```python
$ t = gpst.gpsdatetime()
$ t.wk00()
```	
- day00 : calcul du début du jour

```python
$ t.day00()
```	
- h00 : calcul du début de l'heure

```python
$ t.h00()
```	
- m00 : calcul du début de la minute

```python	
$ t.m00()
```	


## Impression de dates

### Impression de dates complètes

La fonction print permet d'afficher tous les attributs d'un objet \tgps. 

```python
$ t1 = gpst.gpsdatetime() # appel a just_now implicite
$ print(t1) # on aurait pu utiliser t1.print() ou t1.print_dates()
Gpsdatetime (version 2016-06-27)

s1970 : 1480272707.601859
YYYY_MM_DD : 2016/11/27  
HH:MM:SS : 18:51:47.601858854
GPS week : 1925
Day of week : 0 (SUN)
Second of week : 67907.601858854 
Second of day : 67907.601858854  
session : s
Modified Julian Date : 57719.785968  
Julian Date : 2457720.285968
YYYY : 2016  DOY : 332
GMST (dec. hour) : 23.337554
GAST (dec. hour) : 23.337429
Eq. of Equinoxes (dec. hour) : -0.000125
```


### Impression de chaînes formatées

Différentes fonctions permettent de créer des chaînes de caractères à des formats spécifiques utiles en géodésie. 



| Nom de la fonction  | Type de format     | Format |
| :--------------- |:---------------| -----|
|st_iso_epoch()  | Format iso | yyyy-mm-ddThh:mm:ssZ |
|st_pyephem_epoch() | Format d'entrée du package pyephem | yyyy/mm/dd hh:mm:ss.s |
|st_snx_epoch() | Format Sinex | yy:ddd:sssss |

Jacques Beilin, ENSG/PEGMMT, 28 août 2023 

